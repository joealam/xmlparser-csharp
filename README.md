# README #

C# XML parser designed to work in a commonly used pattern which allows different parsing by implementing a derived class of the parser.

## How to use? ##
Include the XMLParser.cs class in your C# project, and create a class which inherits from it and implements the BeginNode and EndNode functions to perform the required behaviour.
See the examples for more guidance.

## Licence ##
The code is released under the MIT License, see LICENCE.txt for full information.