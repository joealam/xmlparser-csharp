﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLParser
{
    class SimpleOutputParser : XMLParser
    {
        public override void BeginNode(string name)
        {
            Console.WriteLine("Begin:\t" + name);
        }

        public override void EndNode(string name)
        {
            Console.WriteLine("End:\t" + name);
        }
    }
}
