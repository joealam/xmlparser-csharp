﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLParser
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "..\\..\\test.xml";

            Console.WriteLine("-----------------------------------------------------------------");
            Console.WriteLine("--------------------- Simple Output Parser ----------------------");
            Console.WriteLine("-----------------------------------------------------------------");
            XMLParser x = new SimpleOutputParser();
            x.Parse(filename);

            Console.WriteLine("-----------------------------------------------------------------");
            Console.WriteLine("--------------------- Tabbed Output Parser ----------------------");
            Console.WriteLine("-----------------------------------------------------------------");

            x = new TabbedOutputParser();
            x.Parse(filename);

            Console.ReadKey();
        }
    }
}
