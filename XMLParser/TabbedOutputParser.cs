﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLParser
{
    class TabbedOutputParser : XMLParser
    {
        public override void BeginNode(string name)
        {
            for (int i = 0; i < m_NumTabs; i++)
            {
                Console.Write("\t");
            }
            Console.Write(name);
            Console.Write("\n");
            m_NumTabs++;

            string data;
            if (GetDataString(out data))
            {
                for (int i = 0; i < m_NumTabs; i++)
                {
                    Console.Write("\t");
                }
                Console.Write("\"" + data + "\"");
                Console.Write("\n");
            }
        }

        public override void EndNode(string name)
        {
            m_NumTabs--;
        }

        private int m_NumTabs = 0;
    }
}
