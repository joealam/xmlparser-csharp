﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XMLParser
{
    /// <summary>
    /// This class may be used as the base of a specific XML parsing implementation. It will automatically run through the document and call the BeginNode
    /// and EndNode functions for each node.
    /// Note: The first node (i.e. document node) will not be included.
    /// </summary>
    abstract class XMLParser
    {
        public XMLParser()
        {
            m_CurrentNode = null;
        }

        /// <summary>
        /// Parse the specified document and call the functions as appropriate.
        /// </summary>
        /// <param name="filename">The filename of the document to parse</param>
        public void Parse(string filename)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filename);
            ProcessLevel(doc.DocumentElement);
        }

        //----------------------------------------------------------------------------------------------------
        // GetAttribute Methods
        //----------------------------------------------------------------------------------------------------

        public bool GetAttributeString(string name, out string str)
        {
            // Default to null
            str = null;

            try
            {
                str = m_CurrentNode.Attributes.GetNamedItem(name).Value;
                return true;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public bool GetAttributeInt(string name, out int i)
        {
            // Default to 0
            i = 0;

            try
            {
                return Int32.TryParse(m_CurrentNode.Attributes.GetNamedItem(name).Value, out i);
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public bool GetAttributeFloat(string name, out float f)
        {
            // Default to 0
            f = 0.0f;

            try
            {
                return float.TryParse(m_CurrentNode.Attributes.GetNamedItem(name).Value, out f);
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public bool GetAttributeBool(string name, out bool b)
        {
            // Default to false
            b = false;

            try
            {
                return Boolean.TryParse(m_CurrentNode.Attributes.GetNamedItem(name).Value, out b);
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public bool GetAttributeUnsignedInt(string name, out uint i)
        {
            // Default to 0
            i = 0;

            try
            {
                return UInt32.TryParse(m_CurrentNode.Attributes.GetNamedItem(name).Value, out i);
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        //----------------------------------------------------------------------------------------------------
        // GetData Methods
        //----------------------------------------------------------------------------------------------------

        public bool GetDataString(out string str)
        {
            // Default to null
            str = null;

            try
            {
                if (m_CurrentNode.HasChildNodes && m_CurrentNode.FirstChild.NodeType == XmlNodeType.Text)
                {
                    str = m_CurrentNode.FirstChild.Value;
                    return true;
                }
                else return false;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public bool GetDataInt(out int i)
        {
            // Default to 0
            i = 0;

            try
            {
                if (m_CurrentNode.HasChildNodes && m_CurrentNode.FirstChild.NodeType == XmlNodeType.Text)
                {
                    return Int32.TryParse(m_CurrentNode.FirstChild.Value, out i);
                }
                else return false;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public bool GetDataFloat(out float f)
        {
            // Default to 0
            f = 0.0f;

            try
            {
                if (m_CurrentNode.HasChildNodes && m_CurrentNode.FirstChild.NodeType == XmlNodeType.Text)
                {
                    return float.TryParse(m_CurrentNode.FirstChild.Value, out f);
                }
                else return false;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public bool GetDataBool(out bool b)
        {
            // Default to 0
            b = false;

            try
            {
                if (m_CurrentNode.HasChildNodes && m_CurrentNode.FirstChild.NodeType == XmlNodeType.Text)
                {
                    return Boolean.TryParse(m_CurrentNode.FirstChild.Value, out b);
                }
                else return false;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public bool GetDataUnsignedInt(out uint i)
        {
            // Default to 0
            i = 0;

            try
            {
                if (m_CurrentNode.HasChildNodes && m_CurrentNode.FirstChild.NodeType == XmlNodeType.Text)
                {
                    return UInt32.TryParse(m_CurrentNode.FirstChild.Value, out i);
                }
                else return false;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        //----------------------------------------------------------------------------------------------------
        // Virtual Begin and End Node methods
        //----------------------------------------------------------------------------------------------------

        // Called when a new node is opened
        public abstract void BeginNode(string name);

        // Called when a node is closed
        public abstract void EndNode(string name);

        //----------------------------------------------------------------------------------------------------
        // Privates
        //----------------------------------------------------------------------------------------------------

        // Process a level of nodes.
        // Called recursively to cover the entire document
        private void ProcessLevel(XmlNode node)
        {
            if (node == null) return;

            foreach (XmlNode n in node.ChildNodes)
            {
                // In this particular procedure, data nodes are not considered to be nodes, but are instead the data belonging to their parent
                if (n.NodeType == XmlNodeType.Text) continue;

                m_CurrentNode = n;

                BeginNode(n.Name);
                ProcessLevel(n);
                EndNode(n.Name);
            }
        }

        // Stores the currently active node, which is then used to get data or attributes
        private XmlNode m_CurrentNode;
    }
}
